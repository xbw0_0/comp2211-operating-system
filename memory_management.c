#include "memory_management.h"

int NODE_SIZE=40;
int FILLING_SIZE=8;
FreeNode* HEAD_NODE_0; 

// Align the beginning
int get_align_size(int size)
{
    return FILLING_SIZE+((((size-1) >> 3) << 3));
}


/** Connect two nodes
 * Case 1: If there is no pre-node, return 0;
 * Case 2: Tf there is pre-node, Add the current node after pre-node; node_size + pre.node
 */
int link_two_node(FreeNode* Node) {
    if (Node == 0 || Node->pre == 0)
    {
	    return 0;
    }

    if (Node->pre->if_free==1) {
        Node->pre->size += NODE_SIZE + Node->size;
        Node->pre->next = Node->next;
        if (Node->next) {
            Node->next->pre = Node->pre;
        }
        return 1;
    }
    return 0;
}


/**
 * Malloc function()
 * Input: Node struct, pointer
 * Output: target pointer site
 */
void* _malloc(int size) {
    size =get_align_size(size);

    FreeNode* p = HEAD_NODE_0;
    FreeNode* previous_node = 0;
    
    //Store the pointer(if free, upgrade to pre_node)
    while (p) {
        if (p->if_free && p->size >= size) {
            break;
        }
        previous_node = p;
        p = p->next;
    }

    void* target_p = 0;

    /**
    *Case 1: If there is enough space left to store the two nodes separately
                create a free node
    *Case 2: If no enough space, create new memory
     */
    
    if (p) {
        if (p->size - size >= FILLING_SIZE + NODE_SIZE) {
            {
                if (p) {
                    FreeNode* NEW_NODE = (FreeNode*)(p->datasection_point + size);
                    NEW_NODE->size = p->size - size - NODE_SIZE;
                    NEW_NODE->if_free = 1;
                    NEW_NODE->datasection_point = (char*)NEW_NODE + NODE_SIZE;
                    NEW_NODE->pre = p;
                    NEW_NODE->next = p->next;
                   
                    if (p->next!=0) {
                        p->next->pre = NEW_NODE;
                        p->next = NEW_NODE;
                    }
                    p->size = size;
                    p->if_free = 0;
                }
            }
        }
        else {
            p->if_free = 0;
        }
        target_p = p->datasection_point;
    }
    else {
        char* temp_p = sbrk(NODE_SIZE + size);
        if (temp_p == (void*)-1) {
            target_p= 0;
        }
        else{
            FreeNode* Node = (FreeNode*)temp_p;
            Node->size = size;
            Node->pre = previous_node;
            Node->next = 0;
            Node->if_free = 0;
            Node->datasection_point = &Node->data;
            if (previous_node) {
                previous_node->next = Node;
            }
            if (HEAD_NODE_0 == 0) {
                HEAD_NODE_0 = Node;
            }
            target_p=Node->datasection_point;
        }
        
    }

    return target_p;
}


void _free(void* ptr) {

    FreeNode* Node = 0;

    char* temp = (char*)ptr;

    //If free memory is successful, the pointer needs to point the node starts;
    //else, return 
    if (temp > (char*)HEAD_NODE_0 && temp < (char*)sbrk(0)) {
        Node=(FreeNode*)(temp - NODE_SIZE);
    }
    else
    {
	    Node=0;
	    return;
    }

    Node->if_free = 1;

    //Case 1: connect two node;
    //Case 2: the next node is free, connect
    if (link_two_node(Node)) {
        Node = Node->pre;
    }
    if (Node->next && Node->next->if_free) {
        link_two_node(Node->next);
    }
    
    if (Node->next == 0) {
        if (Node == HEAD_NODE_0) {
            HEAD_NODE_0 = 0;
        }
        FreeNode* p = Node->pre;
        if (p) {
            p->next = 0;
        }
        sbrk(-(Node->size + NODE_SIZE));
    }
}


