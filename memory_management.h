#ifndef MEMORY_MANAGEMENT_H
#define MEMORY_MANAGEMENT_H


#include "kernel/types.h"
#include "user/user.h"



typedef struct node {
    int if_free;
    int size;   
    struct node* next;   
    struct node* pre;   
    void* datasection_point;  
    double none_; 
    char data[1];
} FreeNode;


void* _malloc(int size);

void _free(void* ptr);

int link_two_node(FreeNode* Node);


#endif 
