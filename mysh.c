#include "kernel/types.h"
#include "kernel/fcntl.h"
#include "user/user.h"

#define MAX_CMD 30  //maximum number of commands
#define MAX_BUFFER 254  //maximum number of buffer
#define MAX_LEN 50   //maximum number of string length

/* Global argument */
char buff[MAX_BUFFER];
char command_argv[MAX_CMD][MAX_LEN];


int read_input()
{
/**
 * Input: buff(accepted string)
 * Output: Store buff contents in a list (no 'enter')
 */
    memset(buff, 0, MAX_BUFFER);
    read(0, buff, MAX_BUFFER);
    buff[strlen(buff) - 1] = '\0';
    return strlen(buff);
}


void parser_input()
{
    /**
     * Input: buff[] and command_argv[]
     * command_argv[]: Divide the words in the buff[] list, 
     *                 when a space is appeared, take out all the previous content 
     *                 and put it on the latest line of the command list
     */
    for(int i = 0; i < MAX_CMD; i++){
        for(int j = 0; j<MAX_LEN; j++){
            command_argv[i][j] = 0;
        }
    }
    int buf_len = strlen(buff);
    int row = 0;
    int j = 0;
    // Create 'command_argv' cmd
    for(int i = 0; i < strlen(buff); i++){
        if(buff[i]==' '){
            if(j != 0){
                command_argv[row][j] = '\0';
                ++row;
                j = 0;
            }
        }
        else if (i==buf_len-1 && j!=0)
        {
            command_argv[row][j++] = buff[i];
            command_argv[row][j] = '\0';
            ++row;
            j = 0; 
        }
        else{
            command_argv[row][j++] = buff[i];
        }
        
    }
    
}



/* Run command */
int run_cmd(){
    //'cd' command
    if (strcmp(command_argv[0], "cd") == 0){
        chdir(command_argv[1]);
        return 1;
    }
    //'exit' command
    if (strcmp(command_argv[0], "exit") == 0){
        exit(0);
        return 0;
    }
    //'redirect' command
    /**
     * Function: x > A.txt
     *       Write the string x to the A.txt file; 
     *       if the file does not exist, create it.
     */
    for(int i=0; i<MAX_CMD; i++){
        if(strcmp(command_argv[i], ">") == 0){
            if(fork() == 0){
                close(1);   //Turn off the Channel(output) to the screen
                open(command_argv[i+1], O_WRONLY|O_CREATE|O_TRUNC); //execute permission on file

                char * argv[MAX_CMD]={0};
                for ( int k=0;k<i;k++){
                    argv[k]=(char*)malloc(MAX_LEN*sizeof(char));  
                    strcpy(argv[k],command_argv[k]);
                }
                exec(argv[0],argv);
                exit(0);
            }else{
                wait(0);
            }
            return 1;
        }
        /**
         * Function: x < A.txt
         *       File descriptors are mapped to standard output 
         *       
        */
        if(strcmp(command_argv[i], "<") == 0){
            if (fork()==0){
                close(0);   //Turn off the Channel(input) from keyboard
                open(command_argv[i+1], O_RDONLY);

                char * argv[MAX_CMD]={0};
                for ( int k=0;k<i;k++){
                    argv[k]=(char*)malloc(MAX_LEN*sizeof(char));  
                    strcpy(argv[k],command_argv[k]);
                }
                exec(argv[0],argv);
                exit(0);
            }
            else
            {
                wait(0);
            }
            return 1;
        }
        /**
         * Function: Continue to write the contents of the file
         */
        if(strcmp(command_argv[i],">>")==0)
        {
            if (fork()==0)
            {
                close(1);
                open(command_argv[i+1], O_WRONLY|O_CREATE);
           
                char * argv[MAX_CMD]={0};
                for ( int k=0;k<i;k++){
                    argv[k]=(char*)malloc(MAX_LEN*sizeof(char));  
                    strcpy(argv[k],command_argv[k]);
                }
                exec(argv[0],argv); 
                exit(0);
            }
            else
            {
                wait(0);
            }
            
            return 1;
        }
        //Pipe command
        /**
         * stdout A -> stdin B ( A | B )
         * p[0] is read port, p[1] is write port
         */
        if(strcmp(command_argv[i], "|") == 0){
            char* argv_left[MAX_CMD] = {0};
            char* argv_right[MAX_CMD] = {0};
            int left_c=0;
            int right_c=0;
            for(int k=0;k<MAX_CMD;k++)
            {
                if(k<i)
                {
                    argv_left[left_c]=(char*)malloc(MAX_LEN*sizeof(char));
                    strcpy(argv_left[left_c++],command_argv[k]);
                }
                else if(k>i)
                {
                    argv_right[right_c]=(char*)malloc(MAX_LEN*sizeof(char));  
                    strcpy(argv_right[right_c++],command_argv[k]);
                }
            }
            int p[2];
            if(pipe(p) >= 0){
                for(int i = 0; i < 2; i++){
                    //Subprocess writes to pipe
                    if(fork() == 0){
                        if(i > 0){
                            close(0);    
                            dup(p[0]);
                        }
                        if(i < 1){ 
                            dup(p[1]);
                        }
                        close(p[0]);
                        close(p[1]);
                        if(i==0)
                            exec(argv_left[0], argv_left);
                        else if (i==1)
                        {
                            exec(argv_right[0], argv_right); 
                        }
                        exit(0);
                    }
                }
                close(p[0]);
                close(p[1]);
                wait(0);
                wait(0);
            }
            if(pipe(p) < 0)
                break;
            return 1;
        }

    }
    
    //When the child process executes a file
    if(fork() == 0){
        char * argv[MAX_CMD]={0};
        for ( int k=0;k<MAX_CMD;k++){
            if(command_argv[k][0]==0){break;}

            argv[k]=(char*)malloc(MAX_LEN*sizeof(char));  
            strcpy(argv[k],command_argv[k]);
        }
        exec(argv[0],argv);
        exit(0);
    }else{
        wait(0);
    }
    return 1;
    


}


int main(int argc, char *argv[])
{
    while(1)
    {
        printf(">>>");
        int res=1;
        read_input();
        parser_input();
        res=run_cmd();
        if (res==0)
            break;
    }

    exit(0);
    return 0;
}